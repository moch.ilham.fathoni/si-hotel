@extends('layouts.admin')

@section('title')
Kamar
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('kamar.index') }}">Kamar</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><strong>Form Edit Data Kamar</strong></div>
                    <form class="form-horizontal" method="POST" action="{{ route('kamar.update', $kamar->id) }}">
                        @method('PATCH')
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="id_tipe_kamar">Pilih Tipe</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="id_tipe_kamar" name="id_tipe_kamar" required>
                                        @foreach($tipe as $key => $item)
                                        <option value="{{ $item->id }}" @php if ( $item->id == $kamar->id_tipe_kamar ) { echo 'selected="selected"'; } @endphp>{{ $item->tipe_kamar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="nomor_kamar">No Kamar</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="nomor_kamar" type="text" name="nomor_kamar" value="{{ $kamar->nomor_kamar }}" autocomplete="nomor_kamar" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="lantai">Lantai</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="lantai" type="text" name="lantai" value="{{ $kamar->lantai }}" autocomplete="lantai" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="status">Status</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="status" type="text" name="status" value="{{ $kamar->status }}" autocomplete="status" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Simpan</button>
                            <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection