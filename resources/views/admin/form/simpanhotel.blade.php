@extends('layouts.admin')

@section('title')
Hotel
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('hotel.index') }}">Hotel</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><strong>Form Input Data Hotel</strong></div>
                    <form class="form-horizontal" method="POST" action="{{ route('hotel.store') }}">
                    @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="id_kelurahan">Pilih Kelurahan</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="id_kelurahan" name="id_kelurahan" required>
                                        @foreach($kelurahan as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->nama_kecamatan }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="id_kamar">Pilih Kamar</label>
                                <div class="col-md-9">
                                    <select class="form-control" id="id_kamar" name="id_kamar" required>
                                        @foreach($kamar as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->tipe->tipe_kamar }} / {{ $item->nomor_kamar }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="nama_hotel">Nama Hotel</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="nama_hotel" type="text" name="nama_hotel" placeholder="Masukkan Nama.." autocomplete="nama_hotel" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="jenis_hotel">Jenis Hotel</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="jenis_hotel" type="text" name="jenis_hotel" placeholder="Jenis.." autocomplete="jenis_hotel" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="bank">Nama Bank</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="bank" type="text" name="bank" placeholder="Masukkan Nama Bank.." autocomplete="bank" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="norek">No Rekening</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="norek" type="text" name="norek" placeholder="Masukkan No Rekening.." autocomplete="norek" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Simpan</button>
                            <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection