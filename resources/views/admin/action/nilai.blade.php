<ul class="nav flex-column">
    @if (!$model->nilai)
    <li class="nav-item">
        <a href="{{ route('nilai.show', $model->id) }}" class="nav-link">
            <svg class="c-icon">
                <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-pencil') }}"></use>
            </svg> <small>Buat Nilai</small>
        </a>
    </li>
    @else
    <li class="nav-item">
        <a href="{{ route('nilai.edit', $model->nilai->id) }}" class="nav-link">
            <svg class="c-icon">
                <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-pencil') }}"></use>
            </svg> <small>Ubah Nilai</small>
        </a>
    </li>
    @endif
</ul>