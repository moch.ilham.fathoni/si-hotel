<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Hotel extends Model
{
    use HasFactory;

    protected $table = 'hotel';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id_kelurahan', 'id_kamar', 'id_user', 'nama_hotel', 'jenis_hotel', 'bank', 'norek',
    ];

    /**
     * Get the kelurahan that owns the hotel.
     */
    public function kelurahan()
    {
        return $this->belongsTo(Kelurahan::class, 'id_kelurahan', 'id');
    }

    /**
     * Get the kamar.
     */
    public function kamar()
    {
        return $this->belongsTo(Kamar::class, 'id_kamar', 'id');
    }

    /**
     * Get the nilai.
     */
    public function nilai()
    {
        return $this->belongsTo(Nilai::class, 'id', 'id_hotel');
    }
}
