<ul class="nav flex-column">
    <li class="nav-item">
        <a href="{{ route('kamar.edit', $model->id) }}" class="nav-link">
            <svg class="c-icon">
                <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-pencil') }}"></use>
            </svg> <small>Ubah</small>
        </a>
    </li>
    <li class="nav-item">
        <form action="{{ route('kamar.destroy', $model->id)}}" method="post" id="kamar_delete">
            @csrf
            @method('DELETE')
            <a href="javascript:{}" onclick="return deleteFunction(this)" class="nav-link">
                <svg class="c-icon">
                    <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-trash') }}"></use>
                </svg> <small>Hapus</small>
            </a>
        </form>
    </li>
</ul>