<table class="table table-bordered table-sm">
<tr>
    <td>Check In</td>
    <td>{{ $model->check_in }}</td>
</tr>
<tr>
    <td>Check Out</td>
    <td>
        {{ $model->check_out }}
    </td>
</tr>
</table>
