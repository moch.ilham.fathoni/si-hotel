<ul class="nav flex-column">
    <li class="nav-item">
        <a href="{{ route('reservasi.print', [$model->nama, $model->email_tamu, $model->hp, $model->check_in, $model->check_out, $model->jumlah_kamar, $model->jumlah_tamu, $model->id_hotel]) }}" class="nav-link">
            <svg class="c-icon">
                <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-print') }}"></use>
            </svg> <small>Print</small>
        </a>
    </li>
    <li class="nav-item">
        <form action="{{ route('reservasi.destroy', $model->id)}}" method="post" id="reservasi_delete">
            @csrf
            @method('DELETE')
            <a href="javascript:{}" onclick="return deleteFunction(this)" class="nav-link">
                <svg class="c-icon">
                    <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-trash') }}"></use>
                </svg> <small>Hapus</small>
            </a>
        </form>
    </li>
</ul>