@extends('layouts.admin')

@section('title')
Inisialisasi Nilai
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('nilai.index') }}">Nilai Kriteria</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"> </i>List Hotel
                        <div class="card-header-actions">
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @elseif (session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('error') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Hotel / Tipe / Kamar</th>
                                    <th>Jarak</th>
                                    <th>Harga</th>
                                    <th>Fasilitas</th>
                                    <th>Actions&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection

@push('scripts')
<script>
    $(function() {
        var paketTable = $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: 'data/nilai'
            },
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', orderable: false, searchable: false },
                    { data: 'hotel', name: 'hotel' },
                    { data: 'jarak', name: 'jarak' },
                    { data: 'harga', name: 'harga' },
                    { data: 'fasilitas', name: 'fasilitas' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                    order: [[1, 'asc']]
        });
    });
</script>
@endpush