<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHotelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hotel', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_kelurahan');
            $table->unsignedBigInteger('id_kamar');
            $table->unsignedBigInteger('id_user');
            $table->string('nama_hotel');
            $table->string('jenis_hotel');
            $table->timestamps();
            $table->foreign('id_kelurahan')
                  ->references('id')
                  ->on('kelurahan')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_kamar')
                  ->references('id')
                  ->on('kamar')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_user')
                  ->references('id')
                  ->on('users')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hotel');
    }
}
