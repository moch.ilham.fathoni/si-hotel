<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\KriteriaJarak;
use App\Models\KriteriaHarga;
use App\Models\KriteriaFasilitas;
use App\Models\Reservasi;
use Yajra\Datatables\Datatables;

class DashboardController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $data = Hotel::paginate(10);
        $jarak = KriteriaJarak::all();
        $harga = KriteriaHarga::all();
        $fasilitas = KriteriaFasilitas::all();

        if($request->filled('jarak') || $request->filled('harga') || $request->filled('fasilitas')) {
            $arr = array();

            if($request->filled('jarak')) {
                $arr['id_jarak'] = $request->get('jarak');
            }
            if($request->filled('harga')) {
                $arr['id_harga'] = $request->get('harga');
            }
            if($request->filled('fasilitas')) {
                $arr['id_fasilitas'] = $request->get('fasilitas');
            }

            $data = Hotel::leftJoin('inisialisasi_nilai', function($join) {
                            $join->on('hotel.id', '=', 'inisialisasi_nilai.id_hotel');
                         })
                         ->where($arr)
                         ->paginate(10);
        }

        return view('hotel', compact('data', 'jarak', 'harga', 'fasilitas'))->render();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function detail($id)
    {
        $data = Hotel::find($id);
        return view('detail', compact('data'))->render();
    }

    public function reservasi(Request $request)
    {
        $request->validate([
            'email_tamu'    => ['required'],
            'check_in'      => ['required'],
            'check_out'     => ['required'],
            'jumlah_kamar'  => ['required', 'integer'],
            'jumlah_tamu'   => ['required', 'integer'],
            'pesan'         => ['required', 'string'],
            'id_hotel'      => ['required'],
        ]);

        try {
            $hotel = Hotel::find($request->get('id_hotel'));
            $datetime1 = strtotime($request->get('check_in'));
            $datetime2 = strtotime($request->get('check_out'));
            $days = (int)(($datetime2 - $datetime1)/86400);
            Reservasi::create(['email_tamu'     => $request->get('email_tamu'),
                               'check_in'       => $request->get('check_in'),
                               'check_out'      => $request->get('check_out'),
                               'jumlah_kamar'   => $request->get('jumlah_kamar'),
                               'jumlah_tamu'    => $request->get('jumlah_tamu'),
                               'pesan'          => $request->get('pesan'),
                               'id_hotel'       => $request->get('id_hotel'),
                               'status'         => 'Order',
                               'nama'           => $request->get('nama'),
                               'hp'             => $request->get('hp'),
                               'total'          => $hotel->kamar->tipe->harga * $request->get('jumlah_kamar') * $days,
            ]);

            return redirect()->route('hotel.detail', ['id' => $request->get('id_hotel')]);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function list()
    {
        return view('admin.reservasi');
    }

    public function getData()
    {
        $data = Reservasi::all();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.reservasi', compact('model'))->render();
                         })
                         ->addColumn('tanggal', function ($model) {
                            return view('admin.action.tanggal', compact('model'))->render();
                         })
                         ->addColumn('jumlah', function ($model) {
                            return view('admin.action.jumlah', compact('model'))->render();
                         })
                         ->editColumn('id_hotel', function ($model) {
                            return $model->hotel->nama_hotel . ' / ' . $model->hotel->kamar->tipe->tipe_kamar;
                         })
                         ->editColumn('total', function ($model) {
                            return number_format($model->total, 2, ',', '.');
                         })
                         ->rawColumns(['tanggal', 'jumlah', 'action'])
                         ->make(true);
    }

    public function destroy($id)
    {
        try {
            $reservasi = Reservasi::find($id);
            $msg = 'Reservasi telah berhasil dihapus!';
            $reservasi->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function print($nama, $email, $hp, $in, $out, $kamar, $tamu, $id)
    {
        $hotel = Hotel::find($id);
        return view('print', compact('nama', 'email', 'hp', 'in', 'out', 'kamar', 'tamu', 'hotel'))->render();
    }
}
