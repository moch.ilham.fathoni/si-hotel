@extends('layouts.admin')

@section('title')
Tipe Kamar
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('tipe.index') }}">Tipe Kamar</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><strong>Form Input Data Tipe Kamar</strong></div>
                    <form class="form-horizontal" method="POST" action="{{ route('tipe.store') }}" enctype="multipart/form-data">
                    @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="tipe_kamar">Tipe Kamar</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="tipe_kamar" type="text" name="tipe_kamar" placeholder="Masukkan Tipe.." autocomplete="tipe_kamar" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="deskripsi">Deskripsi</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5" placeholder="Deskripsi.." required></textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fotos[]">Foto 1</label>
                                <div class="col-md-9">
                                    <input type="file" id="fotos[]" name="fotos[]" accept="image/*" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fotos[]">Foto 2</label>
                                <div class="col-md-9">
                                    <input type="file" id="fotos[]" name="fotos[]" accept="image/*" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fotos[]">Foto 3</label>
                                <div class="col-md-9">
                                    <input type="file" id="fotos[]" name="fotos[]" accept="image/*" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fasilitas_kamar">Fasilitas Kamar</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="fasilitas_kamar" type="text" name="fasilitas_kamar" placeholder="Masukkan Fasilitas.." autocomplete="fasilitas_kamar" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="harga">Harga</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="harga" type="number" name="harga" step="0.01" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="jumlah">Jumlah</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="jumlah" type="number" name="jumlah" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Simpan</button>
                            <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection