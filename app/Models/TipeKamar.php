<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TipeKamar extends Model
{
    use HasFactory;

    protected $table = 'tipe_kamar';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'tipe_kamar', 'deskripsi', 'fasilitas_kamar', 'harga', 'jumlah',
    ];

    /**
     * Get the photos.
     */
    public function photos()
    {
        return $this->hasMany(Foto::class, 'id_tipe_kamar', 'id');
    }
}
