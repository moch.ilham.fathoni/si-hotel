<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Nilai;
use App\Models\KriteriaJarak;
use App\Models\KriteriaHarga;
use App\Models\KriteriaFasilitas;
use Yajra\Datatables\Datatables;

class NilaiController extends Controller
{
     /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.nilai');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_hotel'  => ['required'],
            'id_jarak'      => ['required'],
            'id_harga'    => ['required'],
            'id_fasilitas'   => ['required'],
        ]);

        try {
            Nilai::create(['id_hotel'       => $request->get('id_hotel'),
                           'id_jarak'       => $request->get('id_jarak'),
                           'id_harga'       => $request->get('id_harga'),
                           'id_fasilitas'   => $request->get('id_fasilitas'),
            ]);

            return redirect()->route('nilai.index')->with('status', 'Nilai kriteria telah berhasil disimpan');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $hotel = Hotel::find($id);
        $jarak = KriteriaJarak::all();
        $harga = KriteriaHarga::all();
        $fasilitas = KriteriaFasilitas::all();
        return view('admin.form.simpannilai', compact('hotel', 'jarak', 'harga', 'fasilitas'))->render();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $nilai = Nilai::find($id);
        $jarak = KriteriaJarak::all();
        $harga = KriteriaHarga::all();
        $fasilitas = KriteriaFasilitas::all();
        return view('admin.form.editnilai', compact('nilai', 'jarak', 'harga', 'fasilitas'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_jarak'      => ['required'],
            'id_harga'    => ['required'],
            'id_fasilitas'   => ['required'],
        ]);

        $nilai = Nilai::find($id);
        $nilai->id_jarak = $request->get('id_jarak');
        $nilai->id_harga = $request->get('id_harga');
        $nilai->id_fasilitas = $request->get('id_fasilitas');
        $nilai->save();

        $msg = 'Data nilai kriteria hotel telah berhasil diubah!';
        return redirect()->route('nilai.index')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getData()
    {
        $data = Hotel::all();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.nilai', compact('model'))->render();
                         })
                         ->addColumn('hotel', function ($model) {
                            return $model->nama_hotel . ' / ' . $model->kamar->tipe->tipe_kamar . ' / ' . $model->kamar->nomor_kamar;
                         })
                         ->addColumn('jarak', function ($model) {
                            if ($model->nilai) {
                                return $model->nilai->jarak->nama;
                            }
                            return '-';
                         })
                         ->addColumn('harga', function ($model) {
                            if ($model->nilai) {
                                return $model->nilai->harga->nama;
                            }
                            return '-';
                         })
                         ->addColumn('fasilitas', function ($model) {
                            if ($model->nilai) {
                                return $model->nilai->fasilitas->nama;
                            }
                            return '-';
                         })
                         ->make(true);
    }
}
