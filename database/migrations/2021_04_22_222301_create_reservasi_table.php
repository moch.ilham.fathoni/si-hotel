<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateReservasiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservasi', function (Blueprint $table) {
            $table->id();
            $table->string('email_tamu');
            $table->date('check_in');
            $table->date('check_out');
            $table->integer('jumlah_kamar');
            $table->integer('jumlah_tamu');
            $table->text('pesan');
            $table->unsignedBigInteger('id_hotel');
            $table->string('status');
            $table->integer('total');
            $table->timestamps();
            $table->foreign('id_hotel')
                  ->references('id')
                  ->on('hotel')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservasi');
    }
}
