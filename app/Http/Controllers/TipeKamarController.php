<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TipeKamar;
use App\Models\Foto;
use Yajra\Datatables\Datatables;

class TipeKamarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.tipekamar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.form.simpantipekamar');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'tipe_kamar'        => ['required', 'string'],
            'deskripsi'         => ['required'],
            'fotos'             => ['required'],
            'fasilitas_kamar'   => ['required', 'string'],
            'harga'             => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'jumlah'            => ['required', 'integer'],
        ]);
        try {
            $tipe = TipeKamar::create(['tipe_kamar'     => $request->get('tipe_kamar'),
                                       'deskripsi'      => $request->get('deskripsi'),
                                       'fasilitas_kamar'=> $request->get('fasilitas_kamar'),
                                       'harga'          => $request->get('harga'),
                                       'jumlah'         => $request->get('jumlah'),
                    ]);
            
            if ($request->hasfile('fotos')) {
                foreach ($request->file('fotos') as $foto) {
                    $path = 'foto/tipe/kamar/' . $tipe->id;
                    $foto->move($path, $foto->getClientOriginalName());

                    $file = Foto::create(['nama'            => $foto->getClientOriginalName(),
                                          'id_tipe_kamar'   => $tipe->id,
                    ]);
                }
            }

            return redirect()->route('tipe.index')->with('status', 'Tipe kamar telah berhasil ditambahkan');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tipe = TipeKamar::find($id);
        return view('admin.form.edittipekamar', compact('tipe'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'tipe_kamar'        => ['required', 'string'],
            'deskripsi'         => ['required'],
            'fasilitas_kamar'   => ['required', 'string'],
            'harga'             => 'nullable|regex:/^\d*(\.\d{2})?$/',
            'jumlah'            => ['required', 'integer'],
        ]);

        $tipe = TipeKamar::find($id);
        $tipe->tipe_kamar = $request->get('tipe_kamar');
        $tipe->deskripsi = $request->get('deskripsi');
        $tipe->fasilitas_kamar = $request->get('fasilitas_kamar');
        $tipe->harga = $request->get('harga');
        $tipe->jumlah = $request->get('jumlah');
        // if ($request->hasFile('foto')) {
        //     $foto = $request->file('foto');
        //     $path = 'foto/tipe/kamar/' . $tipe->id;
	    //     $foto->move($path, $foto->getClientOriginalName());
        //     $tipe->foto = $foto->getClientOriginalName();
        // }
        $tipe->save();

        $msg = 'Data tipe kamar ' . $tipe->tipe_kamar . ' telah berhasil diubah!';
        return redirect()->route('tipe.index')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $tipe = TipeKamar::find($id);
            $msg = 'Tipe kamar ' . $tipe->tipe_kamar . ' telah berhasil dihapus!';
            $tipe->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getData()
    {
        $data = TipeKamar::all();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('foto', function ($model) {
                            return view('admin.action.foto', compact('model'))->render();
                         })
                         ->addColumn('action', function ($model) {
                            return view('admin.action.tipekamar', compact('model'))->render();
                         })
                         ->rawColumns(['foto', 'action'])
                         ->make(true);
    }
}
