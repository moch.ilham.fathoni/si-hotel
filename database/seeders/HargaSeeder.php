<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class HargaSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\KriteriaHarga::create([
            'nama'	=> 'Mahal'
        ]);
        \App\Models\KriteriaHarga::create([
            'nama'	=> 'Sedang'
        ]);
        \App\Models\KriteriaHarga::create([
            'nama'	=> 'Murah'
        ]);
    }
}
