<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class FasilitasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\KriteriaFasilitas::create([
            'nama'	=> 'Mewah'
        ]);
        \App\Models\KriteriaFasilitas::create([
            'nama'	=> 'Sedang'
        ]);
        \App\Models\KriteriaFasilitas::create([
            'nama'	=> 'Ekonomi'
        ]);
    }
}
