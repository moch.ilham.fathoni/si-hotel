<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Kamar extends Model
{
    use HasFactory;

    protected $table = 'kamar';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id_tipe_kamar', 'nomor_kamar', 'lantai', 'status',
    ];

    /**
     * Get the tipe that owns the kamar.
     */
    public function tipe()
    {
        return $this->belongsTo(TipeKamar::class, 'id_tipe_kamar', 'id');
    }
}
