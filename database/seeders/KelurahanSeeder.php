<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KelurahanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kelurahan')->insert([
            [
                'nama_kecamatan'  => 'Dum Barat',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kepulauan',
            ],
            [
                'nama_kecamatan'  => 'Dum Timur',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kepulauan',
            ],
            [
                'nama_kecamatan'  => 'Kampung Baru',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kota',
            ],
            [
                'nama_kecamatan'  => 'Klabala',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kota',
            ],
            [
                'nama_kecamatan'  => 'Klablim',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Timur',
            ],
            [
                'nama_kecamatan'  => 'Klademak',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong',
            ],
            [
                'nama_kecamatan'  => 'Klagete',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Malaimsimsa',
            ],
            [
                'nama_kecamatan'  => 'Klaligi',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Manoi',
            ],
            [
                'nama_kecamatan'  => 'Klasaman',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Klaurung',
            ],
            [
                'nama_kecamatan'  => 'Klawasi',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Barat',
            ],
            [
                'nama_kecamatan'  => 'Klawuyuk',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Timur',
            ],
            [
                'nama_kecamatan'  => 'Malaingkedi',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Malaimsimsa',
            ],
            [
                'nama_kecamatan'  => 'Malanu',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Utara',
            ],
            [
                'nama_kecamatan'  => 'Malasilen',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Utara',
            ],
            [
                'nama_kecamatan'  => 'Malawei',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Manoi',
            ],
            [
                'nama_kecamatan'  => 'Matalamagi',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Utara',
            ],
            [
                'nama_kecamatan'  => 'Raam',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kepulauan',
            ],
            [
                'nama_kecamatan'  => 'Remu Selatan',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Manoi',
            ],
            [
                'nama_kecamatan'  => 'Remu Utara',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong',
            ],
            [
                'nama_kecamatan'  => 'Rufei',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Barat',
            ],
            [
                'nama_kecamatan'  => 'Saoka',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Maladum Mes',
            ],
            [
                'nama_kecamatan'  => 'Sawagumu',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Utara',
            ],
            [
                'nama_kecamatan'  => 'Soop',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Sorong Kepulauan',
            ],
            [
                'nama_kecamatan'  => 'Tanjung Kasuari',
                'alamat'     => 'Kota Sorong, Provinsi Papua Barat, Indonesia',
                'distrik'    => 'Maladum Mes',
            ],
        ]);
    }
}
