@extends('layouts.admin')

@section('title')
Nilai
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('nilai.index') }}">Nilai</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header"><strong>Form Input Nilai Kriteria Hotel</strong></div>
                    <form class="form-horizontal" method="POST" action="{{ route('nilai.store') }}">
                    @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-5 col-form-label" for="id_hotel">Hotel / Tipe / Kamar</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="id_hotel" name="id_hotel" required>
                                        <option value="{{ $hotel->id }}">{{ $hotel->nama_hotel . ' / ' . $hotel->kamar->tipe->tipe_kamar . ' / ' . $hotel->kamar->nomor_kamar }}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 col-form-label" for="id_jarak">Kriteria Jarak</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="id_jarak" name="id_jarak" required>
                                        @foreach($jarak as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 col-form-label" for="id_harga">Kriteria Harga</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="id_harga" name="id_harga" required>
                                        @foreach($harga as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-5 col-form-label" for="id_fasilitas">Kriteria Fasilitas</label>
                                <div class="col-md-7">
                                    <select class="form-control" id="id_fasilitas" name="id_fasilitas" required>
                                        @foreach($fasilitas as $key => $item)
                                        <option value="{{ $item->id }}">{{ $item->nama }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Simpan</button>
                            <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection