@extends('layouts.admin')

@section('title')
Hotel
@endsection

@section('subheader')
<li class="breadcrumb-item">Data</li>
<li class="breadcrumb-item" active><a href="{{ route('reservasi.index') }}">Reservasi</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"> </i>List Reservasi
                        <div class="card-header-actions">
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @elseif (session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('error') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama</th>
                                    <th>Email</th>
                                    <th>No HP</th>
                                    <th>Tanggal</th>
                                    <th>Jumlah</th>
                                    <th>Pesan</th>
                                    <th>Nama Hotel / Tipe</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Actions&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteFunction(button) {
        if(confirm('Are you sure you want to delete this ?')) {
            button.parentNode.submit();
            return false;
        } else {
            return false;
        }
    }
</script>
<script>
    $(function() {
        var paketTable = $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: '{!! route('get.reservasi') !!}',
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', orderable: false, searchable: false },
                    { data: 'nama', name: 'nama' },
                    { data: 'email_tamu', name: 'email_tamu' },
                    { data: 'hp', name: 'hp' },
                    { data: 'tanggal', name: 'tanggal' },
                    { data: 'jumlah', name: 'jumlah' },
                    { data: 'pesan', name: 'pesan' },
                    { data: 'id_hotel', name: 'id_hotel' },
                    { data: 'status', name: 'status' },
                    { data: 'total', name: 'total' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                    order: [[1, 'asc']]
        });
    });
</script>
@endpush