@extends('layouts.admin')

@section('title')
Tipe Kamar
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('tipe.index') }}">Tipe Kamar</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header"><strong>Form Edit Data Tipe Kamar</strong></div>
                    <form class="form-horizontal" method="POST" action="{{ route('tipe.update', $tipe->id) }}" enctype="multipart/form-data">
                        @method('PATCH')
                        @csrf
                        <div class="card-body">
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="tipe_kamar">Tipe Kamar</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="tipe_kamar" type="text" name="tipe_kamar" placeholder="Masukkan Tipe.." autocomplete="tipe_kamar" value="{{ $tipe->tipe_kamar }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="deskripsi">Deskripsi</label>
                                <div class="col-md-9">
                                    <textarea class="form-control" id="deskripsi" name="deskripsi" rows="5" placeholder="Deskripsi.." required>{{ $tipe->deskripsi }}</textarea>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fotos[]">Foto 1</label>
                                <div class="col-md-9">
                                    <input type="file" id="fotos[]" name="fotos[]" accept="image/*">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="fasilitas_kamar">Fasilitas Kamar</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="fasilitas_kamar" type="text" name="fasilitas_kamar" placeholder="Masukkan Fasilitas.." autocomplete="fasilitas_kamar" value="{{ $tipe->fasilitas_kamar }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="harga">Harga</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="harga" type="number" name="harga" step="0.01" value="{{ number_format($tipe->harga, 2) }}" required>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-3 col-form-label" for="jumlah">Jumlah</label>
                                <div class="col-md-9">
                                    <input class="form-control" id="jumlah" type="number" name="jumlah" value="{{ $tipe->jumlah }}" required>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button class="btn btn-sm btn-primary" type="submit"> Simpan</button>
                            <button class="btn btn-sm btn-danger" type="reset"> Reset</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection