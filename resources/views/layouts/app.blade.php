<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

<title>SI | Hotel Kota Sorong</title>

<!-- Google fonts -->
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800|Old+Standard+TT' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Raleway:300,500,800' rel='stylesheet' type='text/css'>
<!-- font awesome -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

<!-- bootstrap -->
<link rel="stylesheet" href="{{ asset('dashboard/assets/bootstrap/css/bootstrap.min.css') }}" />
<!-- uniform -->
<link type="text/css" rel="stylesheet" href="{{ asset('dashboard/assets/uniform/css/uniform.default.min.css') }}" />
<!-- animate.css -->
<link rel="stylesheet" href="{{ asset('dashboard/assets/wow/animate.css') }}" />
<!-- gallery -->
<link rel="stylesheet" href="{{ asset('dashboard/assets/gallery/blueimp-gallery.min.css') }}">

<!-- favicon -->
<link rel="shortcut icon" href="{{ asset('img/logo-kota-sorong.png') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('img/logo-kota-sorong.png') }}" type="image/x-icon">
<link rel="stylesheet" href="{{ asset('dashboard/assets/style.css') }}">
</head>
<body id="home">

<!-- top 
  <form class="navbar-form navbar-left newsletter" role="search">
        <div class="form-group">
          <input type="text" class="form-control" placeholder="Enter Your Email Id Here">
        </div>
        <button type="submit" class="btn btn-inverse">Subscribe</button>
    </form>
 top -->

<!-- header -->
<nav class="navbar  navbar-default" role="navigation">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="{{ url('/') }}"><img style="width: 60px; height: 60px;" src="{{ asset('img/logo-kota-sorong.png') }}"  alt="logo"></a>
      <a class="navbar-brand" href="{{ url('/') }}"><br><b>KOTA SORONG</b></a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
      
      <ul class="nav navbar-nav">        
        <li><a href="{{ url('/') }}">Beranda </a></li>
        {{-- <li><a href="rooms-tariff.php">Hotel & Tarif</a></li>         --}}
        <li><a href="{{ route('list') }}">Hotel & Tarif</a></li>
      </ul>
    </div><!-- Wnavbar-collapse -->
  </div><!-- container-fluid -->
</nav>
<!-- header -->

@yield('content')

<footer class="spacer">
        <div class="container">
            <div class="row">
                <div class="col-sm-5">
                    <h4>Kota Sorong</h4>
                    <p>Kota Sorong adalah sebuah kota di Provinsi Papua Barat, Indonesia. Kota ini dikenal dengan sebutan Kota Minyak, di mana Nederlands Nieuw-Guinea Petroleum Maatschappij (NNGPM) mulai melakukan aktivitas pengeboran minyak bumi di Sorong sejak tahun 1935. Sorong adalah kota terbesar di Provinsi Papua Barat serta kota terbesar kedua di Papua Indonesia, setelah Kota Jayapura.</p>
                </div>              
                 
                 <div class="col-sm-3">
                    <h4>Link Terkait</h4>
                    <ul class="list-unstyled">
                        <li><a href="{{ route('list') }}">Hotel & Tarif</a></li>
                    </ul>
                </div>
                 <div class="col-sm-4 subscribe">
                    <h4>Berlangganan</h4>
                    <div class="input-group">
                    <input type="text" class="form-control" placeholder="Enter email id here">
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button">Get Notify</button>
                    </span>
                    </div>
                    <div class="social">
                    <a href="#"><i class="fa fa-facebook-square" data-toggle="tooltip" data-placement="top" data-original-title="facebook"></i></a>
                    <a href="#"><i class="fa fa-instagram"  data-toggle="tooltip" data-placement="top" data-original-title="instragram"></i></a>
                    <a href="#"><i class="fa fa-twitter-square" data-toggle="tooltip" data-placement="top" data-original-title="twitter"></i></a>
                    <a href="#"><i class="fa fa-pinterest-square" data-toggle="tooltip" data-placement="top" data-original-title="pinterest"></i></a>
                    <a href="#"><i class="fa fa-tumblr-square" data-toggle="tooltip" data-placement="top" data-original-title="tumblr"></i></a>
                    <a href="#"><i class="fa fa-youtube-square" data-toggle="tooltip" data-placement="top" data-original-title="youtube"></i></a>
                    </div>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container-->    
    
    <!--/.footer-bottom--> 
</footer>

<div class="text-center copyright">Aplikasi skripsi hotel kota sorong</div>

<a href="#home" class="toTop scroll"><i class="fa fa-angle-up"></i></a>

<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title">title</h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->    
</div>

<script src="{{ asset('dashboard/assets/jquery.js') }}"></script>
<!-- wow script -->
<script src="{{ asset('dashboard/assets/wow/wow.min.js') }}"></script>
<!-- uniform -->
<script src="{{ asset('dashboard/assets/uniform/js/jquery.uniform.js') }}"></script>
<!-- boostrap -->
<script src="{{ asset('dashboard/assets/bootstrap/js/bootstrap.js') }}" type="text/javascript" ></script>
<!-- jquery mobile -->
<script src="{{ asset('dashboard/assets/mobile/touchSwipe.min.js') }}"></script>
<!-- jquery mobile -->
<script src="{{ asset('dashboard/assets/respond/respond.js') }}"></script>
<!-- gallery -->
<script src="{{ asset('dashboard/assets/gallery/jquery.blueimp-gallery.min.js') }}"></script>
<!-- custom script -->
<script src="{{ asset('dashboard/assets/script.js') }}"></script>
@stack('scripts')
</body>
</html>