@extends('layouts.admin')

@section('title')
Hotel
@endsection

@section('subheader')
<li class="breadcrumb-item">Dashboard</li>
<li class="breadcrumb-item" active><a href="{{ route('hotel.index') }}">Hotel</a></li>
@endsection

@section('content')
<div class="container-fluid">
    <div class="fade-in">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header"><i class="fa fa-align-justify"></i>
                        <div class="card-header-actions">
                            <a class="btn btn-primary" style="color: white;" href="{{ route('hotel.create') }}">
                                <svg class="c-icon">
                                    <use xlink:href="{{ asset('coreui/dist/vendors/@coreui/icons/svg/free.svg#cil-medical-cross') }}"></use>
                                </svg>&nbsp;Tambah Hotel
                            </a>
                        </div>
                    </div>
                    <div class="card-body">
                        @if (session('status'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            {{ session('status') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @elseif (session('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ session('error') }}<button class="close" type="button" data-dismiss="alert" aria-label="Close"><span
                                    aria-hidden="true">×</span></button>
                        </div>
                        @endif
                        @if ($errors->any())
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <ul>
                                @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                        <div class="table-responsive">
                        <table id="myTable" class="table table-bordered table-striped table-sm">
                            <thead>
                                <tr>
                                    <th>No</th>
                                    <th>Nama Hotel</th>
                                    <th>Kelurahan</th>
                                    <th>Jenis Hotel</th>
                                    <th>Bank - Norek</th>
                                    <th>Actions&nbsp;</th>
                                </tr>
                            </thead>
                        </table>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>
            </div>
        </div>
        <!-- /.row-->
    </div>
</div>
@endsection

@push('scripts')
<script>
    function deleteFunction(button) {
        if(confirm('Are you sure you want to delete this ?')) {
            button.parentNode.submit();
            return false;
        } else {
            return false;
        }
    }
</script>
<script>
    $(function() {
        var paketTable = $('#myTable').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ajax: {
                url: 'data/hotel'
            },
            columns: [
                    { data: 'DT_RowIndex', name: 'DT_RowIndex', width: '5%', orderable: false, searchable: false },
                    { data: 'nama_hotel', name: 'nama_hotel' },
                    { data: 'id_kelurahan', name: 'id_kelurahan' },
                    { data: 'jenis_hotel', name: 'jenis_hotel' },
                    { data: 'norek', name: 'norek' },
                    { data: 'action', name: 'action', orderable: false, searchable: false }
                ],
                    order: [[1, 'asc']]
        });
    });
</script>
@endpush