@extends('layouts.app')

@section('content')
@php
$first = true;
@endphp
<div class="container">
    <h1 class="title">{{ $data->nama_hotel }}</h1>

    <div id="RoomDetails" class="carousel slide" data-ride="carousel">
        <div class="carousel-inner">
            @foreach($data->kamar->tipe->photos as $key => $foto)
                @if ($first)
                <div class="item active"><img src="{{ asset('foto/tipe/kamar/' . $data->kamar->tipe->id . '/' . $foto->nama) }}" class="img-responsive" alt="slide"></div>
                @else
                <div class="item  height-full"><img src="{{ asset('foto/tipe/kamar/' . $data->kamar->tipe->id . '/' . $foto->nama) }}" class="img-responsive" alt="slide"></div>
                @endif

                @php
                $first = false;
                @endphp
            @endforeach
            {{-- <div class="item  height-full"><img src="images/photos/9.jpg"  class="img-responsive" alt="slide"></div> --}}
        </div>
        <!-- Controls -->
        <a class="left carousel-control" href="#RoomDetails" role="button" data-slide="prev"><i class="fa fa-angle-left"></i></a>
        <a class="right carousel-control" href="#RoomDetails" role="button" data-slide="next"><i class="fa fa-angle-right"></i></a>
    </div>

    <div class="room-features spacer">
        <div class="row">
            <div class="col-sm-12 col-md-5"> 
                <p>{{ $data->kamar->tipe->deskripsi }}</p>
            </div>
            <div class="col-sm-6 col-md-3 amenitites"> 
                <h3>{{ $data->jenis_hotel }}</h3>    
                <ul>
                    <li>Lokasi: {{ $data->kelurahan->alamat }}</li>
                    <li>Distrik: {{ $data->kelurahan->distrik }}</li>
                    <li>Fasilitas: {{ $data->kamar->tipe->fasilitas_kamar }} </li>
                </ul>
            </div>  
            {{-- <div class="col-sm-3 col-md-2">
            <div class="size-price">Size<span>44 sq</span></div>
            </div> --}}
            <div class="col-sm-6 col-md-4">
                <div class="size-price">Harga<span>Rp. {{ number_format($data->kamar->tipe->harga, 2, ',', '.') }}</span></div>
            </div>
            <div class="col-sm-12 col-md-5"> 
                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#myModal">
                    Pesan
                </button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h3 class="modal-title" id="myModalLabel">Form Reservasi</h3>
        </div>
        <form id="formReservasi" method="POST" action="{{ route('reservasi.store') }}">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label for="nama">Nama :</label>
                <input type="text" class="form-control" id="nama" name="nama" required>
            </div>
            <div class="form-group">
                <label for="email_tamu">Email :</label>
                <input type="email" class="form-control" id="email_tamu" name="email_tamu" required>
            </div>
            <div class="form-group">
                <label for="hp">No HP :</label>
                <input type="text" class="form-control" id="hp" name="hp" required>
            </div>
            <div class="form-group">
                <label for="check_in">Check In :</label>
                <input type="date" class="form-control" id="check_in" name="check_in" required>
            </div>
            <div class="form-group">
                <label for="check_out">Check Out :</label>
                <input type="date" class="form-control" id="check_out" name="check_out" required>
            </div>
            <div class="form-group">
                <label for="jumlah_kamar">Jumlah Kamar :</label>
                <input type="number" class="form-control" id="jumlah_kamar" name="jumlah_kamar" required>
            </div>
            <div class="form-group">
                <label for="jumlah_tamu">Jumlah Tamu :</label>
                <input type="number" class="form-control" id="jumlah_tamu" name="jumlah_tamu" required>
            </div>
            <div class="form-group">
                <label for="pesan">Pesan :</label>
                <textarea class="form-control" id="pesan" name="pesan" rows="3" required></textarea>
            </div>
            <input type="hidden" id="id_hotel" name="id_hotel" value="{{ $data->id }}">
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function doFunction() {
        var nama = document.getElementById("nama").value;
        var email = document.getElementById("email_tamu").value;
        var hp = document.getElementById("hp").value;
        var checkin = document.getElementById("check_in").value;
        var checkout = document.getElementById("check_out").value;
        var kamar = document.getElementById("jumlah_kamar").value;
        var tamu = document.getElementById("jumlah_tamu").value;
        var id = document.getElementById("id_hotel").value;
        var url = '{{ route("reservasi.print", [":nama", ":email", ":hp", ":checkin", ":checkout", ":kamar", ":tamu", ":id"]) }}';
        url = url.replace(':nama', nama);
        url = url.replace(':email', email);
        url = url.replace(':hp', hp);
        url = url.replace(':checkin', checkin);
        url = url.replace(':checkout', checkout);
        url = url.replace(':kamar', kamar);
        url = url.replace(':tamu', tamu);
        url = url.replace(':id', id);
        window.open(url, '_blank');
    }
    $('#formReservasi').submit(function() {
        doFunction();
        return true; // return false to cancel form action
    });
</script>
@endpush