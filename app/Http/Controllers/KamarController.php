<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Kamar;
use App\Models\TipeKamar;
use Yajra\Datatables\Datatables;

class KamarController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.kamar');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tipe = TipeKamar::all();
        return view('admin.form.simpankamar', compact('tipe'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_tipe_kamar' => ['required'],
            'nomor_kamar'   => ['required', 'string', 'unique:kamar'],
            'lantai'        => ['required', 'string'],
            'status'        => ['required', 'string'],
        ]);

        try {
            Kamar::create(['id_tipe_kamar'  => $request->get('id_tipe_kamar'),
                           'nomor_kamar'    => $request->get('nomor_kamar'),
                           'lantai'         => $request->get('lantai'),
                           'status'         => $request->get('status'),
            ]);

            return redirect()->route('kamar.index')->with('status', 'Kamar telah berhasil ditambahkan');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $kamar = Kamar::find($id);
        $tipe = TipeKamar::all();
        return view('admin.form.editkamar', compact('kamar', 'tipe'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_tipe_kamar' => ['required'],
            'nomor_kamar'   => ['required', 'string'],
            'lantai'        => ['required', 'string'],
            'status'        => ['required', 'string'],
        ]);

        $kamar = Kamar::find($id);
        $kamar->id_tipe_kamar = $request->get('id_tipe_kamar');
        $kamar->nomor_kamar = $request->get('nomor_kamar');
        $kamar->lantai = $request->get('lantai');
        $kamar->status = $request->get('status');
        $kamar->save();

        $msg = 'Data kamar telah berhasil diubah!';
        return redirect()->route('kamar.index')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $kamar = Kamar::find($id);
            $msg = 'Data kamar telah berhasil dihapus!';
            $kamar->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getData()
    {
        $data = Kamar::all();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.kamar', compact('model'))->render();
                         })
                         ->editColumn('id_tipe_kamar', function ($model) {
                            return $model->tipe->tipe_kamar;
                         })
                         ->make(true);
    }
}
