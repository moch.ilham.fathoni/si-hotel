<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Nilai extends Model
{
    use HasFactory;

    protected $table = 'inisialisasi_nilai';

    /**
    * The attributes that are mass assignable.
    *
    * @var array
    */
    protected $fillable = [
        'id_hotel', 'id_jarak', 'id_harga', 'id_fasilitas',
    ];

    public function hotel()
    {
        return $this->belongsTo(Hotel::class, 'id_hotel', 'id');
    }

    public function jarak()
    {
        return $this->belongsTo(KriteriaJarak::class, 'id_jarak', 'id');
    }

    public function harga()
    {
        return $this->belongsTo(KriteriaHarga::class, 'id_harga', 'id');
    }

    public function fasilitas()
    {
        return $this->belongsTo(KriteriaFasilitas::class, 'id_fasilitas', 'id');
    }
}
