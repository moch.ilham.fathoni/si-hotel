<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class JarakSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\KriteriaJarak::create([
            'nama'	=> 'Jauh'
        ]);
        \App\Models\KriteriaJarak::create([
            'nama'	=> 'Sedang'
        ]);
        \App\Models\KriteriaJarak::create([
            'nama'	=> 'Dekat'
        ]);
    }
}
