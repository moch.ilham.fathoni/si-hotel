<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/hotel-tarif', [App\Http\Controllers\DashboardController::class, 'index'])->name('list');
Route::get('/hotel/detail/{id}', [App\Http\Controllers\DashboardController::class, 'detail'])->name('hotel.detail');
Route::get('/list/reservasi', [App\Http\Controllers\DashboardController::class, 'list'])->name('reservasi.index');
Route::post('/reservasi', [App\Http\Controllers\DashboardController::class, 'reservasi'])->name('reservasi.store');
Route::get('data/reservasi', [App\Http\Controllers\DashboardController::class, 'getData'])->name('get.reservasi');
Route::delete('/reservasi/{id}', [App\Http\Controllers\DashboardController::class, 'destroy'])->name('reservasi.destroy');
Route::get('print/{nama}/{email}/{hp}/{in}/{out}/{kamar}/{tamu}/{id}', [App\Http\Controllers\DashboardController::class, 'print'])->name('reservasi.print');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HotelController::class, 'index'])->name('home');
Route::resource('tipe', App\Http\Controllers\TipeKamarController::class);
Route::resource('kamar', App\Http\Controllers\KamarController::class);
Route::resource('hotel', App\Http\Controllers\HotelController::class);
Route::resource('nilai', App\Http\Controllers\NilaiController::class);
Route::get('data/tipe-kamar', [App\Http\Controllers\TipeKamarController::class, 'getData']);
Route::get('data/kamar', [App\Http\Controllers\KamarController::class, 'getData']);
Route::get('data/hotel', [App\Http\Controllers\HotelController::class, 'getData']);
Route::get('data/nilai', [App\Http\Controllers\NilaiController::class, 'getData']);