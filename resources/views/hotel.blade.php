@extends('layouts.app')

@section('content')
<div class="container">
    <h2>Hotel & Tarif</h2>
    <div class="row">
        <div class="col-12">
            <form class="form-inline" method="GET" action="{{ route('list') }}">
                <div class="form-group">
                    <label class="mr-1" for="jarak">Jarak</label>
                    <select class="form-control" id="jarak" name="jarak">
                        <option value=""> --- </option>
                        @foreach($jarak as $key => $item)
                        <option value="{{ $item->id }}" @php if ( $item->id == request()->get('jarak') ) { echo 'selected="selected"'; } @endphp>{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="mr-1" for="harga">Harga</label>
                    <select class="form-control" id="harga" name="harga">
                        <option value=""> --- </option>
                        @foreach($harga as $key => $item)
                        <option value="{{ $item->id }}" @php if ( $item->id == request()->get('harga') ) { echo 'selected="selected"'; } @endphp>{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label class="mx-1" for="fasilitas">Fasilitas</label>
                    <select class="form-control" id="fasilitas" name="fasilitas">
                        <option value=""> --- </option>
                        @foreach($fasilitas as $key => $item)
                        <option value="{{ $item->id }}" @php if ( $item->id == request()->get('fasilitas') ) { echo 'selected="selected"'; } @endphp>{{ $item->nama }}</option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <label><br><br></label>
                    <button style="margin-top: 20px;" class="btn btn-success" type="submit"> Filter</button>
                </div>
            </form>
        </div>
        <div class="col-12">
        <p><br></p>
        </div>
        <div class="col-12">
            <div class="row">
                @foreach($data as $key => $item)
                    <div class="col-sm-6 wowload fadeInUp">
                        <div class="rooms">
                            <img src="{{ asset('foto/tipe/kamar/' . $item->kamar->tipe->id . '/' . $item->kamar->tipe->photos->first()->nama) }}" class="img-responsive">
                            <div class="info">
                                <h3>{{ $item->nama_hotel }}</h3>
                                <p> {{ $item->kamar->tipe->deskripsi }}</p>
                                <a href="{{ route('hotel.detail', $item->id) }}" class="btn btn-default">Check Details</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="text-center">
        {{ $data->links() }}
    </div>
</div>
@endsection
