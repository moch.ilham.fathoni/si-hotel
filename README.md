## Cara Instalasi

Aplikasi web ini dibuat menggunakan framework laravel. Jika ingin menjalankan aplikasi ini, lakukan konfigurasi terlebih dahulu 
dengan mengikuti langkah - langkah berikut ini:

- Install git (jika belum terinstall).
- Install composer (jika belum terinstall).
- Install Node.js (jika belum terinstall).
- Buka terminal dan menuju direktori yang akan dipakai untuk menyimpan aplikasi ini.
- Jalankan perintah ini: git clone https://gitlab.com/moch.ilham.fathoni/si-hotel.git.
- Setelah berhasil mengclone project, masuk ke direktori project tersebut (cd si-hotel/).
- Install project dependencies dengan cara jalankan perintah ini: composer install.
- Install NPM dependencies dengan cara jalankan perintah ini: npm install.
- Copy .env file atau jalankan perintah ini: cp .env.example .env.
- Generate encryption key untuk aplikasi dengan cara menjalankan perintah ini: php artisan key:generate.
- Buat database mysql dengan nama dbhotel.
- Pada file .env ubah db_username dan db_password sesuai konfigurasi mysql pada localhost.
- Lakukan migrasi database dengan menjalankan perintah ini: php artisan migrate.
- Isi data default ke database yg dibuat programmer dengan menjalankan perintah ini: php artisan db:seed.
- Setelah menjalanlan perintah db:seed maka dapat login ke halaman admin dengan username: admin dan password: 12345678.

Jika masih belum berhasil menjalankan aplikasi ini, silahkan hubungi team developer terima kasih.