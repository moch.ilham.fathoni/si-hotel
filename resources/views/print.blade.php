<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>

<body>
@php
$datetime1 = strtotime($in);
$datetime2 = strtotime($out);
$days = (int)(($datetime2 - $datetime1)/86400);
@endphp
<div style="width: 480px; height: 560px; border: 1px solid dark; background-color: #E3E2E2;">
<br>
<u><h2 class="text-center">Bukti Pemesanan Hotel</h2></u>
<br>
<div class="container">
    <div class="row align-items-center">
        <div class="col-2">
            Nama
        </div>
        <div class="col-10">: {{ $nama }}</div>
        <div class="col-2">
            Email
        </div>
        <div class="col-10">: {{ $email }}</div>
        <div class="col-2">
            No HP
        </div>
        <div class="col-10">: {{ $hp }}</div>
        <div class="col-2">
            Hotel / Tipe / No
        </div>
        <div class="col-10">: {{ $hotel->nama_hotel . ' / ' . $hotel->kamar->tipe->tipe_kamar . ' / ' . $hotel->kamar->nomor_kamar }}</div>
        <div class="col-2">
            Check In
        </div>
        <div class="col-10">: {{ date("d-m-Y", strtotime($in)) }}</div>
        <div class="col-2">
            Check Out
        </div>
        <div class="col-10">: {{ date("d-m-Y", strtotime($out)) }}</div>
        <div class="col-2">
            Jumlah Kamar
        </div>
        <div class="col-10">: {{ $kamar }}</div>
        <div class="col-2">
            Jumlah Tamu
        </div>
        <div class="col-10">: {{ $tamu }}</div>
        <div class="col-2">
            Harga
        </div>
        <div class="col-10">: Rp. {{ number_format($hotel->kamar->tipe->harga, 2, ',', '.') }}</div>
        <div class="col-2">
            Total
        </div>
        <div class="col-10">: Rp. {{ number_format($hotel->kamar->tipe->harga * $kamar * $days, 2, ',', '.') }}</div>
        <div class="col-2">
            Pembayaran ke
        </div>
        <div class="col-10">: {{ $hotel->bank . ' - ' . $hotel->norek }}</div>
    </div>
</div>
</div>
<script>
    window.focus();
    window.print();
</script>
</body>

</html>