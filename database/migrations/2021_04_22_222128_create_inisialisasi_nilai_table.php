<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInisialisasiNilaiTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('inisialisasi_nilai', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('id_hotel');
            $table->unsignedBigInteger('id_jarak');
            $table->unsignedBigInteger('id_harga');
            $table->unsignedBigInteger('id_fasilitas');
            $table->timestamps();
            $table->foreign('id_hotel')
                  ->references('id')
                  ->on('hotel')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_jarak')
                  ->references('id')
                  ->on('kriteria_jarak')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_harga')
                  ->references('id')
                  ->on('kriteria_harga')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
            $table->foreign('id_fasilitas')
                  ->references('id')
                  ->on('kriteria_fasilitas')
                  ->onUpdate('cascade')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('inisialisasi_nilai');
    }
}
