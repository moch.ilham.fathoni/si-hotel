<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Hotel;
use App\Models\Kamar;
use App\Models\Kelurahan;
use Yajra\Datatables\Datatables;
use Illuminate\Support\Facades\Auth;

class HotelController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.hotel');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $kamar = Kamar::all();
        $kelurahan = Kelurahan::all();
        return view('admin.form.simpanhotel', compact('kamar', 'kelurahan'))->render();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'id_kelurahan'  => ['required'],
            'id_kamar'      => ['required'],
            'nama_hotel'    => ['required', 'string'],
            'jenis_hotel'   => ['required', 'string'],
        ]);

        try {
            Hotel::create(['id_kelurahan'   => $request->get('id_kelurahan'),
                           'id_kamar'       => $request->get('id_kamar'),
                           'id_user'        => Auth::user()->id,
                           'nama_hotel'     => $request->get('nama_hotel'),
                           'jenis_hotel'    => $request->get('jenis_hotel'),
                           'bank'           => $request->get('bank'),
                           'norek'          => $request->get('norek'),
            ]);

            return redirect()->route('hotel.index')->with('status', 'Hotel telah berhasil ditambahkan');
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        }
        catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $hotel = Hotel::find($id);
        $kamar = Kamar::all();
        $kelurahan = Kelurahan::all();
        return view('admin.form.edithotel', compact('hotel', 'kamar', 'kelurahan'))->render();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'id_kelurahan'  => ['required'],
            'id_kamar'      => ['required'],
            'nama_hotel'    => ['required', 'string'],
            'jenis_hotel'   => ['required', 'string'],
        ]);

        $hotel = Hotel::find($id);
        $hotel->id_kelurahan = $request->get('id_kelurahan');
        $hotel->id_kamar = $request->get('id_kamar');
        $hotel->id_user = Auth::user()->id;
        $hotel->nama_hotel = $request->get('nama_hotel');
        $hotel->jenis_hotel = $request->get('jenis_hotel');
        if ($request->get('bank')) {
            $hotel->bank = $request->get('bank');
        }
        if ($request->get('norek')) {
            $hotel->norek = $request->get('norek');
        }
        $hotel->save();

        $msg = 'Data hotel telah berhasil diubah!';
        return redirect()->route('hotel.index')->with('status', $msg);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        try {
            $hotel = Hotel::find($id);
            $msg = 'Data hotel telah berhasil dihapus!';
            $hotel->delete();
            return back()->with('status', $msg);
        } catch (\Exception $e) {
            return back()->with('error', $e->getMessage());
        } catch (\Throwable $e) {
            return back()->with('error', $e->getMessage());
        }
    }

    public function getData()
    {
        $data = Hotel::all();
        return Datatables::of($data)
                         ->addIndexColumn()
                         ->addColumn('action', function ($model) {
                            return view('admin.action.hotel', compact('model'))->render();
                         })
                         ->editColumn('id_kelurahan', function ($model) {
                            return $model->kelurahan->nama_kecamatan;
                         })
                         ->editColumn('norek', function ($model) {
                            return $model->bank . ' - ' . $model->norek;
                         })
                         ->make(true);
    }
}
